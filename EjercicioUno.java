// FUNCIONA
import java.util.Scanner;

public class EjercicioUno {
    public static void main(String[] args) {
        int numSecreto = (int) (Math.random() * 100 + 1);
        int num;
        boolean comprobar;
        do {
            num = introducirNum();
            if (num == -1){
                System.out.println("¿Te rindes?, la próxima vez será.");
                comprobar = true;
            } else {
                comprobar = comprobarNum(num, numSecreto);
                }
            } while (comprobar == false) ;
    }

//////////////////////////////////////////////////////////////////
    public static int introducirNum() {
        Scanner entrada = new Scanner(System.in);
        int num;
        do {
            System.out.println("Para rendirse introduzca -1.\nAdivina un número entre 1 y 100\nIntroduzca un número:");
            num = entrada.nextInt();
        } while (num < -1 || num > 100);
        return num;
    }
/////////////////////////////////////////////////////////////////
    public static boolean comprobarNum(int num, int numSecreto){
        boolean salir = false;
        if (num > numSecreto){
            System.out.println(num + " es mayor que el número secreto.");
        } else if (num < numSecreto) {
            System.out.println(num + " es menor que el número secreto.");
        } else {
            System.out.println("¡¡¡Has acertado!!! el número secreto es " + numSecreto);
            salir = true;
        }
        return salir;
    }

}
