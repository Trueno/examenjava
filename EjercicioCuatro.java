import com.sun.corba.se.impl.orb.ParserTable;
// FUNCIONA
import java.util.Scanner;

public class EjercicioCuatro {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int tipo = 3;
        String texto = "Introduzca un coeficiente:";
        double resultado;
        System.out.println(texto);
        double coeficienteA = entrada.nextInt();
        if (coeficienteA == 0) {
            tipo = tipo -1;
        }
        System.out.println(texto);
        double coeficienteB = entrada.nextInt();
        if (coeficienteB == 0) {
            tipo = tipo - 1;
        }
        System.out.println(texto);
        double coeficienteC = entrada.nextInt();
        if (coeficienteC == 0) {
            tipo = tipo - 1;
        }
        if (coeficienteA == 0 && coeficienteB==0) {
            System.out.println("Hay dos exponentes o tres iguales a 0 por lo que esto no corresponde a una ecuación.");
        } else if (coeficienteA == 0) {
            resultado = -coeficienteC / coeficienteB;
            System.out.println(resultado);
        } else if (coeficienteA != 0 && coeficienteB != 0 && coeficienteC != 0) {
            double discriminante = coeficienteB * (coeficienteB - 4) * coeficienteA * coeficienteC;
            if (discriminante >= 0) {
                double re = - coeficienteB / (2 * coeficienteA);
                double im = Math.sqrt(Math.abs(discriminante) / (2 * coeficienteA));
                System.out.println(re - im);
                System.out.println(re + im);
            } else {
                System.out.println("No existe solución para esta ecucación dentro del campo de los números reales");
            }
        }
    }
}