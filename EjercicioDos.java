import java.util.Scanner;

public class EjercicioDos {
    public static void main(String[] args) {
        int num = introducirNum();
        esPrimo(num);
    }

    ////////////////////////////////////////////////////
    public static int introducirNum() {
        Scanner entrada = new Scanner(System.in);
        int num;
        do {
            System.out.println("Introduce un número:");
            num = entrada.nextInt();
            return num;
        } while (num < 0);
    }

    ///////////////////////////////////////////////////
    public static void esPrimo(int num) {
        boolean primo = false;
        for (int i = 5; i <= 2; i--) {
            if (num % i == 0) {
                primo = true;
            }
        }
        if (primo == true) {
            System.out.println("El número " + num + " es primo.");
        }
        else {
            System.out.println("El número " + num + " no es primo.");
        }
    }
}
